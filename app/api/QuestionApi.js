import { preguntas, unidades } from "../utils/Data";

const QuestionApi = {
  getTema: (key) => {
    return (async () => {
      var unidad = "";
      unidades.forEach((element) => {
        if (element.num == key) {
          unidad = element.nombre;
        }
      });
      return unidad;
    })();
  },
  getPregunta: (unidad, pregunta) => {
    return (async () => {
      var result = [];
      preguntas.forEach((element) => {
        if (element.unidad == unidad && element.num == pregunta) {
          result = element.pregunta;
        }
      });
      return await result;
    })();
  },
  getNumPregunta: (unidad, pregunta) => {
    return (async () => {
      var result = [];
      preguntas.forEach((element) => {
        if (element.unidad == unidad && element.num == pregunta) {
          result = element.num;
        }
      });
      return await result;
    })();
  },
  getRespuestasOpcion: (unidad, pregunta, id) => {
    return (async () => {
      var result = [];
      preguntas.forEach((element) => {
        if (element.unidad == unidad && element.num == pregunta) {
          if (element.opcion1[0].id == id) {
            result = element.opcion1[0].respuesta;
          }
          if (element.opcion2[0].id == id) {
            result = element.opcion2[0].respuesta;
          }
          if (element.opcion3[0].id == id) {
            result = element.opcion3[0].respuesta;
          }
          if (element.opcion4[0].id == id) {
            result = element.opcion4[0].respuesta;
          }
        }
      });
      return await result;
    })();
  },

  getRespuestasEstados: (unidad, pregunta, id) => {
    return (async () => {
      var result = [];
      preguntas.forEach((element) => {
        if (element.unidad == unidad && element.num == pregunta) {
          if (element.opcion1[0].id == id) {
            result = element.opcion1[0].estado;
          }
          if (element.opcion2[0].id == id) {
            result = element.opcion2[0].estado;
          }
          if (element.opcion3[0].id == id) {
            result = element.opcion3[0].estado;
          }
          if (element.opcion4[0].id == id) {
            result = element.opcion4[0].estado;
          }
        }
      });
      return await result;
    })();
  },

  getRespuestaCorrecta: (unidad, pregunta) => {
    return (async () => {
      var result = [];
      preguntas.forEach((element) => {
        if (element.unidad == unidad && element.num == pregunta) {

          if (element.opcion1[0].estado == 1) {
            result = element.opcion1[0].respuesta;
          }
          if (element.opcion2[0].estado == 1) {
            result = element.opcion2[0].respuesta;
          }
          if (element.opcion3[0].estado == 1) {
            result = element.opcion3[0].respuesta;
          }
          if (element.opcion4[0].estado == 1) {
            result = element.opcion4[0].respuesta;
          }
        }
      });
      return await result;
    })();
  },
  getRespuestas: (unidad, pregunta) => {
    return (async () => {
      var result = [];
      preguntas.forEach((element) => {
        if (element.unidad == unidad && element.num == pregunta) {
          result.push(element.opcion1[0]);
          result.push(element.opcion2[0]);
          result.push(element.opcion3[0]);
          result.push(element.opcion4[0]);
        }
      });

      return await result;
    })();
  },
  getData: (unidad, pregunta) => {
    return (async () => {
      var result = [];
      preguntas.forEach((element) => {
        if (element.unidad == unidad && element.num == pregunta) {
          result = element;
        }
      });
      return await result;
    })();
  },
  getImagen: (unidad, pregunta) => {
    return (async () => {
      var imagen = "";
      preguntas.forEach((element) => {
        if (element.unidad == unidad && element.num == pregunta) {
          imagen = element.ruta_img;
        }
      });
      return imagen;
    })();
  },
};

export default QuestionApi;
