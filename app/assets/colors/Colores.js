export const color_azul_fuert = "#101573";
export const color_azul_medio = "#2E34A6";
export const color_celeste = "#5F88D9";
export const color_rojo_bajo = "#F25757";
export const color_blanco = "#F2F2F2";

export const color_res_corr = "#3CCB76";
export const color_res_incor = "#E90404";
