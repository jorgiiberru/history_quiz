import firebase from 'firebase/app';

const firebaseConfig = {
    apiKey: "AIzaSyAm2Kig4iSFRPY2RYRQo6oDRwD1izabYR4",
    authDomain: "historyquizz-1cd82.firebaseapp.com",
    projectId: "historyquizz-1cd82",
    storageBucket: "historyquizz-1cd82.appspot.com",
    messagingSenderId: "701912218643",
    appId: "1:701912218643:web:2d3c32079e05978892d3e6",
    measurementId: "G-E21T050VKW" 

    /*apiKey: "AIzaSyCGmT0n3QfJflUiEFZ7CI6hl7kdLXlo2Mk",
    authDomain: "history-quizz-4e486.firebaseapp.com",
    projectId: "history-quizz-4e486",
    storageBucket: "history-quizz-4e486.appspot.com",
    messagingSenderId: "622900329255",
    appId: "1:622900329255:web:28bc770801fbb55d918702",
    measurementId: "G-9NPZ326E5F"*/
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);