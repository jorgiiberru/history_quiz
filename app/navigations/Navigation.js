import React from 'react';
import {NavigationContainer} from "@react-navigation/native";
import { createStackNavigator } from '@react-navigation/stack';
import { Icon } from "react-native-elements";
import PrincipalStack from './PrincipalStack';
import Principal from '../screens/Login/Principal';
import Login from '../screens/Login/Login';
import Registro from '../screens/Login/Registro';
import { color_rojo_bajo } from '../assets/colors/Colores';
const Stack = createStackNavigator();

export default function Navigation() {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen 
                    name='principal'
                    component={Principal}
                    options={{
                        headerShown: false,
                    }}     
                /> 
                <Stack.Screen 
                    name='login'
                    component={Login}
                    options={{
                        title: 'Login',
                        headerTintColor:color_rojo_bajo,
                        headerTransparent: true, 
                    }}
                />
                <Stack.Screen 
                    name='registro'
                    component={Registro}
                    options={{  
                        title:"Crea cuenta",
                        headerTintColor:color_rojo_bajo ,
                        headerTransparent: true, }}
                />   
                <Stack.Screen 
                    name='principal-menu'
                    component={PrincipalStack}
                    options={{
                        headerShown: false,
                    }}     
                /> 
            </Stack.Navigator>
        </NavigationContainer>
    )
}

function screenOptions(route, color) {
    let iconName;
  
    switch (route.name) {
      case "home":
            iconName = "home-outline";
            break;
      case "usuario":
        iconName = "account";
        break;
      case "buscar":
        iconName = "magnify";
        break;
      default:
        break;
    }
    return (
      <Icon type="material-community" name={iconName} size={22} color={color_rojo_bajo}/>
    );
  }
  