import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { color_azul_fuert, color_rojo_bajo } from "../assets/colors/Colores";
import Bienvenidos from "../screens/Login/Bienvenidos";

import Preguntas from "../screens/Preguntas/Unidades/Preguntas";
import Question from "../screens/Preguntas/Unidades/Question";
import Home from "../screens/Login/Home";
import Puntaje from "../screens/Preguntas/Unidades/Puntaje";

const Stack = createStackNavigator();
export default function PreguntaStack() {
  return (
    <Stack.Navigator>
      {/* <Stack.Screen
        name="bienvenido"
        component={Bienvenidos}
        options={{
          title: "Bienvenido",
          headerTintColor: color_rojo_bajo,
          headerShown: false,
        }}
      /> */}
      <Stack.Screen
        name="home"
        component={Home}
        options={{
          title: "Bienvenido",
          headerTintColor: color_rojo_bajo,
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="puntaje"
        component={Puntaje}
        options={{
          title: "puntaje",
          headerTintColor: color_rojo_bajo,
          headerShown: false,
        }}
      />

      <Stack.Screen
        name="preguntas"
        component={Preguntas}
        options={{
          title: "preguntas",
          headerTintColor: color_rojo_bajo,
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="question"
        component={Question}
        options={{
          title: "preguntas",
          headerTintColor: color_rojo_bajo,
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
}
