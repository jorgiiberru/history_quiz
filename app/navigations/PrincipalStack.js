import React from 'react'
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { Icon } from "react-native-elements";
import BuscarStack from './BuscarStack';
import UsuarioStack from './UsuarioStack';
import PreguntaStack from './PreguntaStack';
import { color_azul_fuert, color_rojo_bajo } from '../assets/colors/Colores';
import Home from '../screens/Login/Home';
import VerVideo from '../screens/Videos/VerVideo';

const Tab = createBottomTabNavigator();

export default function PrincipalStack() {
    return    (     
        <Tab.Navigator
            initialRouteName="home"
            tabBarOptions={{
                inactiveTintColor   : "#646464",
                activeTintColor     : color_rojo_bajo,
            }}
                screenOptions={({ route }) => ({
                    tabBarIcon: ({ color }) => screenOptions(route, color),
                })}
        >
            <Tab.Screen
                name="home"
                component={PreguntaStack}
                options={{ title: "Home" }}
            />
            <Tab.Screen
                name="usuario"
                component={UsuarioStack}
                options={{ title: "usuario" }}
            />
           {/*  <Tab.Screen
                name="top-10"
                component={VerVideo}
                options={{ title: "Top 10" }}
            />    */}            
            <Tab.Screen
                name="buscar"
                component={BuscarStack}
                options={{ title: "Libro" }}
            />
        </Tab.Navigator> 
   )        
}


function screenOptions(route, color) {
    let iconName;
  
    switch (route.name) {
      case "home":
            iconName = "home-outline";
            break;
      case "usuario":
        iconName = "account";
        break;
      case "buscar":
        iconName = "book-play";
        break;
      case "top-10":
            iconName = "star-face";
        break;
      default:
        break;
    }
    return (
      <Icon type="material-community" name={iconName} size={22} color={color_azul_fuert}/>
    );
  }