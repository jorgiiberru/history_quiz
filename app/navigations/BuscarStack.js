import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { color_rojo_bajo } from "../assets/colors/Colores";
import Buscar from "../screens/Buscar/Buscar";
import Recursos from "../screens/Recursos/Recursos";
import VideoPromocional from "../screens/Recursos/VideoPromocional";
import VideoTutorial from "../screens/Recursos/VideoTutorial";

const Stack = createStackNavigator();
export default function BuscarStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="recursos"
        component={Recursos}
        options={{
          title: "buscar",
          headerTintColor: color_rojo_bajo,
        }}
      />
      <Stack.Screen
        name="Libro"
        component={Buscar}
        options={{
          title: "Libro Historia",
          headerTintColor: color_rojo_bajo,
        }}
      />
      <Stack.Screen
        name="Tutorial"
        component={VideoTutorial}
        options={{
          title: "Video Tutorial",
          headerTintColor: color_rojo_bajo,
        }}
      />
      <Stack.Screen
        name="Promocional"
        component={VideoPromocional}
        options={{
          title: "Video Promocional",
          headerTintColor: color_rojo_bajo,
        }}
      />
    </Stack.Navigator>
  );
}
