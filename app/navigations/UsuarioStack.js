import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import { color_azul_fuert, color_rojo_bajo } from "../assets/colors/Colores";
import Usuario from "../screens/Usuario/Usuario";
import VerPerfil from "../screens/Usuario/VerPerfil";
import Principal from "../screens/Login/Principal";
import Registro from "../screens/Login/Registro";
import Login from "../screens/Login/Login";
import Informacion from "../screens/Usuario/Informacion";

const Stack = createStackNavigator();
export default function UsuarioStack() {
  return (
    <Stack.Navigator>
      
      <Stack.Screen
        name="ver_perfil"
        component={VerPerfil}
        options={{
          title: "pregunta",
          headerTintColor: color_azul_fuert,
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="acerca"
        component={Informacion}
        options={{
          title: "acerca",
          headerTintColor: color_azul_fuert,
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
}
