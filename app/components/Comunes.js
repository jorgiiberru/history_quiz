import React from "react";
import { Image, Text, View } from "react-native";
import { styles } from "../assets/styles/estilos";

export function Cabecera() {
  return (
    <View
      style={{
        backgroundColor: "#F2F2F2",
        height: 70,
        borderBottomStartRadius: 70,
        borderBottomEndRadius: 20,
        borderStartWidth: 3,
        borderEndWidth: 1,
        borderBottomWidth: 7,
        borderColor: "#2E34A6",
      }}
    ></View>
  );
}

export function EncabezadoUnidad(props) {
  const { titulo } = props;
  return (
    <View style={styles.head_pregunta}>
      <View style={styles.view_head_pregunta}>
        <Text style={styles.txt_head_pregunta}>{titulo}</Text>
      </View>
    </View>
  );
}

export function EncabezadoPuntuacion(props) {
  const { pregunta, puntos, tiempo } = props;
  return (
    <View style={styles.view_estado}>
      <View style={{ width: "33%" }}>
        <Text style={styles.text_pregunta}> {pregunta}/10</Text>
      </View>
      <View style={{ width: "33%", flexDirection: "row" }}>
        <Image source={require("./../../assets/img/icons/coin.png")} />
        <Text style={styles.text_pregunta}> {puntos}</Text>
      </View>
      <View style={{ width: "33%", flexDirection: "row" }}>
        <Image source={require("./../../assets/img/icons/reloj.png")} />
        <Text style={styles.text_pregunta}> {tiempo}</Text>
      </View>
    </View>
  );
}



export function ViewPregunta(props) {
  const { pregunta } = props;
  return (
    <View style={styles.pregunta_view}>
      <Text style={styles.pregunta_text}>
        {pregunta} 
      </Text>
    </View>
  );
}
