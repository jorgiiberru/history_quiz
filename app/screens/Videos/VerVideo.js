import { WebView } from "react-native-webview";
import React from "react";

export default function VerVideo() {
  var url =
    "https://drive.google.com/file/d/13IquIyqFB8Z3_LDcFgKw370GCWm5ornf/view?usp=sharing";
  return <WebView source={{ uri: url }} />;
}
