import { FontAwesome } from "@expo/vector-icons";
import React from "react";
import { TouchableOpacity } from "react-native";
import { View, Text, Image } from "react-native";
import {
  color_azul_fuert,
  color_blanco,
  color_celeste,
} from "../../assets/colors/Colores";
import { styles } from "../../assets/styles/estilos";
import Loading from "../../components/Loading";
import { useNavigation } from "@react-navigation/native";

export default function VerPerfil() {
  const navigation = useNavigation();

  const goFunction = () => {
    console.log("DATr");
    navigation.push("acerca");
  };
  return (
    <View>
      <Loading isVisible={false} text="Iniciado Sesión" />

      <View style={{ backgroundcolor: color_azul_fuert }}>
        <View
          style={{
            backgroundColor: color_azul_fuert,
            height: 60,
            width: "100%",
          }}
        ></View>
        <View
          style={{ backgroundColor: color_blanco, height: 60, width: "100%" }}
        ></View>
        <View
          style={{
            backgroundColor: color_celeste,
            height: 80,
            width: 80,
            alignSelf: "center",
            marginTop: -100,
            borderRadius: 80,
          }}
        >
          <Image
            source={require("../../../assets/img/avatares/poseidon.png")}
            style={styles.image_avatar}
          />
        </View>
      </View>
      <View style={{ paddingTop: 20, paddingBottom: 20 }}>
        <View>
          <Text
            style={{ alignSelf: "center", fontSize: 18, fontWeight: "bold" }}
          >
            David Troya
          </Text>
          <Text style={{ alignSelf: "center", fontSize: 14 }}>
            user@gmail.com
          </Text>
        </View>
      </View>
      <View style={{ borderTopWidth: 0.5 }}>
        <DatosEstudiante icono="users" texto="David Troya" />
        <DatosEstudiante icono="envelope-square" texto="user@gmail.com" />
        <DatosEstudiante icono="star" texto="Puntaje" />
        <DatosEstudiante icono="lock" texto="*********************" />
        <DatosEstudiante
          icono="info"
          texto="Acerca HistoryQuizz"
          goFunction={goFunction}
        />
        <DatosEstudiante icono="sign-out" texto="Cerrar Sesiòn" />
      </View>
    </View>
  );
}

function DatosEstudiante(props) {
  const { icono, texto, goFunction } = props;
  return (
    <TouchableOpacity onPress={ goFunction}>
      <View
        style={{
          height: 60,
          borderBottomWidth: 0.5,
          justifyContent: "center",
          alignContent: "center",
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <View style={{ width: "10%" }}>
          <FontAwesome name={icono} size={24} color="black" />
        </View>
        <View style={{ width: "80%" }}>
          <Text>{texto}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
}
