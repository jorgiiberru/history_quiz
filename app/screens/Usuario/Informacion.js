import React from "react";
import { Image } from "react-native";
import { ImageBackground, ScrollView } from "react-native";
import { View, Text } from "react-native";
import { color_blanco, color_rojo_bajo } from "../../assets/colors/Colores";
import { styles } from "../../assets/styles/estilos";

export default function Informacion() {
  return (
    <View style={styles.container_princ}>
      {/*  <ImageBackground
        source={require("../../../assets/img/menu_principal.jpg")}
        style={styles.image_princ}
      > */}
      {/* <View style={{ alignSelf: "center", height: "30%" }}>
          <Image
            style={styles.logo_info}
            source={require("../../../assets/img/logo.png")}
          />
        </View> */}
      {/*  </ImageBackground> */}

      <Image
        style={{ width: 300, height: 300,alignSelf:'center' }}
        source={require("../../../assets/img/logo.png")}
      />
      <Text
        style={{
          paddingHorizontal: 30,
          paddingTop: 0,
          fontSize: 16,
          color: color_blanco,
          textAlign: "center",
        }}
      >
        History Quizz es una aplicación de preguntas y respuestas en donde
        aprenderás Historia de Primero de Bachillerato de manera fácil y
        creativa, consiste en responder una serie de preguntas repartidas en
        diferentes unidades, en un tiempo máximo de 1 minuto y acumular puntos,
        si acumulas la máxima cantidad de puntos podrás estar en el top 10 el
        cual consiste en una lista de los mejores jugadores con las puntuaciones
        más altas. Además, podrás encontrar el texto en pdf para que puedas
        repasar el contenido antes de empezar a jugar.
      </Text>
    </View>
  );
}
