import { FontAwesome, MaterialCommunityIcons } from '@expo/vector-icons'
import {useNavigation} from "@react-navigation/native";
import firebase from 'firebase'
import React, { useState } from 'react'
import { View, Text, StatusBar, Image, TouchableOpacity, ImageBackground, ToastAndroid, ScrollView } from 'react-native'
import { TextInput } from 'react-native-gesture-handler'
import { color_azul_fuert, color_azul_medio, color_blanco, color_rojo_bajo } from '../../assets/colors/Colores'
import { styles } from '../../assets/styles/estilos'
import { Cabecera } from '../../components/Comunes'
import Loading from '../../components/Loading'
import { validateEmail } from '../../utils/validaciones'

export default function Login() {
    const navigation = useNavigation();
    const [formData , setFormData]      = useState(defaultFormValue);
    const [isLoading , setIsLoading]    = useState(false);

    const iniciarSesion= ()=>{
        if( formData.email=='' || formData.password==''){
            ToastAndroid.show("Todos los campos son obligatorios", 1000);
        } else if (!validateEmail(formData.email)) {
            ToastAndroid.show("El email no es correcto", 1000);
        } else{
            setIsLoading(true);
                firebase.
                auth().
                signInWithEmailAndPassword(formData.email,formData.password).
                then(()=>{
                    setIsLoading(false);
                    navigation.navigate("principal-menu");
                    console.log("Entre")
                }).catch((err)=>{
                    setIsLoading(false);
                    ToastAndroid.show("El email o password incorrecto", 1000);
                })
        }
       /*  navigation.navigate("bienvenido"); */
    }

    const crearCuenta= ()=>{
        navigation.navigate("registro");
    }
    const onChange = (e,type)=>{
        setFormData({ ...formData, [type]: e.nativeEvent.text })
    }
    return (
        <View style={styles.container_princ}>
            <Cabecera />
            <View style={{flex:1,flexDirection:'row',alignItems:'center',alignContent:'center',alignSelf:'center'}}>
                <ScrollView>
                    <View style={{alignSelf:'center',height:250,width:'100%',alignItems:'center'}}>
                        <Image                    
                            source= {require('../../../assets/img/logo.png')}
                            style={styles.image}    
                        />
                    </View>                    
                    <View style={styles.view_form_login}>
                    <Loading isVisible={isLoading} text="Iniciando Sesión" />
                    <View style={styles.input}>                    
                        <View style={styles.view_form_input}>
                            <View style={styles.input_icon}>
                                <FontAwesome name="user" size={28} color={color_azul_medio} />
                            </View>
                            <View style={styles.view_input}>
                                <TextInput   
                                    style={styles.text_input_form} 
                                    placeholderTextColor = {color_blanco}
                                    placeholder= 'Email'
                                    onChange={e=>onChange(e,"email")}
                                />
                            </View>
                        </View>
                    </View>
                    <View style={styles.input}>                    
                        <View style={styles.view_form_input}>
                            <View style={styles.input_icon}>
            
                                <MaterialCommunityIcons name="lock" size={28} color={color_azul_medio} />
                            </View>
                            <View style={styles.view_input}>
                                <TextInput   
                                    style={styles.text_input_form} 
                                    placeholderTextColor = {color_blanco}
                                    placeholder= 'Contraseña'
                                    onChange={e=>onChange(e,"password")}
                                    secureTextEntry={true}
                                />
                            </View>
                        </View>
                    </View>
                </View>
                <View style={styles.view_btn_form_login}>
                    <TouchableOpacity 
                            onPress={iniciarSesion}
                            style={styles.touchable_btn}>
                        <View >
                            <Text style={styles.text_input}>
                                Iniciar Sesion
                            </Text>
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={{flexDirection:'row',justifyContent:'center',paddingTop:45,}}> 
                <Text style={{color:color_blanco,paddingRight:5}}>   
                        No tienes una cuenta?    
                    </Text>
                    <TouchableOpacity onPress={crearCuenta}>
                        <Text style={{color:color_rojo_bajo}}>   
                            Regístrate
                        </Text>                
                    </TouchableOpacity>

                </View> 
                </ScrollView>
            </View>
        </View>
    )
}


function defaultFormValue() {
    return {
      email: "",
      password: "",
    };
}