import * as React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { Image, TouchableWithoutFeedback } from "react-native";
import { StyleSheet } from "react-native";
import { Dimensions } from "react-native";
import { Text, View, SafeAreaView } from "react-native";
import { useNavigation } from "@react-navigation/core";

import Carousel from "react-native-snap-carousel";
import {
  color_azul_fuert,
  color_azul_medio,
  color_celeste,
} from "../../assets/colors/Colores";
import { styles, styles_welcome } from "../../assets/styles/estilos";

export default function Home() {
  const [unidad, setUnidad] = useState([]);
  const navigation = useNavigation();

  const goUnidad = (unidad) => {
    let numPregunta = 1;
    let puntuacion = 20;

    console.log(unidad);
    navigation.navigate("preguntas", {
      numPregunta,
      unidad,
      puntuacion,
    });
  };
  const unidades = [
    {
      nombre: "CIVILIZACIONES FLUVIALES DE LA ANTIGUEDAD",
      num: 3,
      ruta: "../../../assets/img/fondo_menu/fondo_1.jpg",
    },
    {
      nombre: "GRECIA LA CUNA DE OCCIDENTE",
      num: 4,
      ruta: "../../../assets/img/fondo_menu/fondo_1.jpg",
    },
    {
      nombre: "LA CIVILIZACION ROMANA",
      num: 5,
      ruta: "../../../assets/img/fondo_menu/fondo_1.jpg",
    },
    {
      nombre: "LA MUJER EN EL ANTIGUO ORIENTE",
      num: 6,
      ruta: "../../../assets/img/fondo_menu/fondo_1.jpg",
    },
  ];

  const renderItem = ({ item }) => {
    return (
      <View>
        <TouchableWithoutFeedback onPress={() => goUnidad(item.num)}>
          <View
            style={{
              backgroundColor: color_celeste,
              borderRadius: 5,
              height: 400,
              padding: 40,
              marginLeft: 25,
              marginRight: 25,
              paddingTop: 20,
              paddingBottom: 20,
              borderRadius: 30,
              borderBottomWidth: 7,
              borderRightWidth: 5,
              borderLeftWidth: 1,
              borderColor: color_azul_medio,
              alignItems: "center",
            }}
          >
            {item.num == 3 && (
              <Image
                style={styles_welcome.image_unidad}
                source={require("../../../assets/img/fondo_menu/fondo_1.jpg")}
              />
            )}
            {item.num == 4 && (
              <Image
                style={styles_welcome.image_unidad}
                source={require("../../../assets/img/fondo_menu/fondo_2.jpg")}
              />
            )}
            {item.num == 5 && (
              <Image
                style={styles_welcome.image_unidad}
                source={require("../../../assets/img/fondo_menu/fondo_3.jpg")}
              />
            )}
            {item.num == 6 && (
              <Image
                style={styles_welcome.image_unidad}
                source={require("../../../assets/img/fondo_menu/fondo_4.jpg")}
              />
            )}

            <Text style={styles_welcome.txt_unidad}>{item.nombre} </Text>
          </View>
        </TouchableWithoutFeedback>
      </View>
    );
  };
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: color_azul_fuert }}>
      <View style={styles.banner_bienvenido}>
        <Text style={styles.txt_bienvenido}>Bienvenido </Text>
      </View>

      <View
        style={{
          flex: 1,
          flexDirection: "row",
          justifyContent: "center",
          paddingTop: 80,
        }}
      >
        <Carousel
          layout={"default"}
          data={unidades}
          sliderWidth={400}
          itemWidth={300}
          renderItem={renderItem}
          onSnapToItem={(index) => setUnidad(index)}
        />
      </View>
    </SafeAreaView>
  );
}

const width = Dimensions.get("window").width;
const height = 160;

const stylesCarr = StyleSheet.create({
  container: {
    position: "relative",
  },
  carousel: {
    width: 100,
    height: 100,
  },
  dotsContainer: {
    position: "absolute",
    bottom: -20,
    width: "100%",
  },
  dot: {
    backgroundColor: "#fff",
  },
});
