import React from 'react'
import { View, Text, StatusBar, Image, TouchableOpacity,StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/core';
import { color_azul_fuert, color_blanco, color_rojo_bajo } from '../../assets/colors/Colores';
import ImageBackground from 'react-native/Libraries/Image/ImageBackground';
import { styles } from '../../assets/styles/estilos';
import { FAB } from 'react-native-paper';

export default function Principal() {
    const navigation = useNavigation();
    const iniciarSesion= ()=>{
        navigation.navigate("login");
    }
    const omitir= ()=>{
        navigation.navigate("principal-menu");
    } 
    const image = { uri: "https://reactjs.org/logo-og.png" };
   
    return (
        <View style={styles.container_princ}>
            <StatusBar style="auto" />
            <ImageBackground source={require('../../../assets/img/menu_principal.jpg')} style={styles.image_princ}>
                <View style={{alignSelf:'center',height:'40%'}}>
                    <Image     
                        style={styles.tinyLogo_princ}               
                        source={require('../../../assets/img/logo.png')}
                    />
                </View>
                <View style={{alignSelf:'center',flexDirection:'row'}}>
                    <TouchableOpacity
                        onPress={iniciarSesion} 
                        style={{width:'70%'}}>
                        <View style={{backgroundColor:color_rojo_bajo,height:50,borderRadius:25,justifyContent:'center'}}>
                            <Text style={{textAlign:'center',fontSize:18,color:color_blanco,fontWeight:'bold'}}>
                                Login
                            </Text>
                        </View>
                    </TouchableOpacity>
                </View>  
                <View style={{position: 'absolute', margin: 16, right: 0,bottom: 0,}}>
                    <TouchableOpacity onPress={omitir}>
                        <Text style={{textAlign:'right',fontSize:18,color:color_blanco,fontWeight:'bold',paddingRight:'20%'}}>Omitir </Text>
                    </TouchableOpacity>
                </View>

            </ImageBackground>
            
        </View>
    )
}

const styless = StyleSheet.create({
    fab: {
      position: 'absolute',
      margin: 16,
      right: 0,
      bottom: 0,
    },
  })