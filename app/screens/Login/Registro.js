import { FontAwesome, MaterialCommunityIcons } from '@expo/vector-icons'
import { useNavigation } from '@react-navigation/core'
import React, { useState } from 'react'
import { View, Text, StatusBar, Image, TouchableOpacity, ImageBackground } from 'react-native'
import { ScrollView, TextInput } from 'react-native-gesture-handler'
import { color_azul_fuert, color_azul_medio, color_blanco, color_rojo_bajo } from '../../assets/colors/Colores'
import { styles } from '../../assets/styles/estilos'
import * as firebase from 'firebase';
import { ToastAndroid } from 'react-native'
import { validateEmail } from '../../utils/validaciones'
import Loading from '../../components/Loading'
import { Cabecera } from '../../components/Comunes'

export default function Registro() {
    const navigation = useNavigation();
    const [formData , setFormData]      = useState(defaultFormValue);
    const [isLoading , setIsLoading]    = useState(false);

    const iniciarSesion= ()=>{
        console.log(formData);
        console.log("Dato")
    }

    const crearCuenta = ()=>{
        if( formData.email=='' || formData.password=='' ||  formData.repeatPassword==''){
            ToastAndroid.show("Todos los campos son obligatorios", 1000);
        } else if (!validateEmail(formData.email)) {
            ToastAndroid.show("El email no es correcto", 1000);
        } else if (formData.password !== formData.repeatPassword) {
            ToastAndroid.show("Las contraseñas tienen que ser iguales", 1000);
        } else if (formData.password.length < 6) {
            ToastAndroid.show("Contraseña debe tener al menos 6 caracteres", 1000);
        } else{
            setIsLoading(true);
                firebase.
                auth().
                createUserWithEmailAndPassword(formData.email,formData.password).
                then((response)=>{
                    setIsLoading(false);
                    navigation.navigate("principal-menu");
                    console.log("Entre")
                }).catch((err)=>{
                    setIsLoading(false);
                    ToastAndroid.show("El email ya esta en uso, registrese con otro", 1000);
                })
        }
    }

    const onChange = (e,type)=>{
        setFormData({ ...formData, [type]: e.nativeEvent.text })
    }
    
    return (
        <View style={styles.container_princ}>
            <Cabecera />
            <View style={{flex:1,flexDirection:'row',alignItems:'center',alignContent:'center',alignSelf:'center'}}>
                <ScrollView>
                    <Loading isVisible={isLoading} text="Creando una cuenta" />

                    <View style={styles.view_form_register}>
                        <View style={styles.input_register}>                    
                            <View style={styles.view_form_input}>
                                <View style={styles.input_icon}>
                                    <MaterialCommunityIcons name="email" size={28} color={color_azul_medio} />
                                </View>
                                <View style={styles.view_input}>
                                    <TextInput   
                                        style={styles.text_input_form} 
                                        placeholderTextColor = {color_blanco}
                                        placeholder= 'Correo'
                                        onChange={e=>onChange(e,"email")}

                                    />
                                </View>
                            </View>
                        </View>
                        <View style={styles.input_register}>                    
                            <View style={styles.view_form_input}>
                                <View style={styles.input_icon}>
                                    <MaterialCommunityIcons name="lock" size={28} color={color_azul_medio} />
                                </View>
                                <View style={styles.view_input}>
                                    <TextInput   
                                        style={styles.text_input_form} 
                                        placeholderTextColor = {color_blanco}
                                        placeholder= 'Contraseña'
                                        onChange={e=>onChange(e,"password")}
                                        secureTextEntry={true}


                                    />
                                </View>
                            </View>
                        </View>
                        <View style={styles.input}>                    
                            <View style={styles.view_form_input}>
                                <View style={styles.input_icon}>
                                    <MaterialCommunityIcons name="lock" size={28} color={color_azul_medio} />
                                </View>
                                <View style={styles.view_input}>
                                    <TextInput   
                                        style={styles.text_input_form} 
                                        placeholderTextColor = {color_blanco}
                                        placeholder= 'Confirmar contraseña'
                                        onChange={e=>onChange(e,"repeatPassword")}
                                        secureTextEntry={true}

                                    />
                                </View>
                            </View>
                        </View>
                        <View style={styles.view_btn_form_register}>
                            <TouchableOpacity 
                                    onPress={crearCuenta}
                                    style={styles.touchable_btn}>
                                <View >
                                    <Text style={styles.text_input}>
                                        Crear cuenta
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>   
            </View>
                        
        </View>
    )
}

function defaultFormValue() {
    return {
/*       nombre: "",
 */   email: "",
      password: "",
      repeatPassword: "",
    };
}