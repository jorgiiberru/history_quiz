import { useNavigation } from "@react-navigation/core";
import React, { useEffect, useState } from "react";
import { View, Text, Image } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { styles, styles_welcome } from "../../assets/styles/estilos";
import Loading from "../../components/Loading";
import { preguntas } from "../../utils/Data";

export default function Bienvenidos() {
  const navigation = useNavigation();
  const [isLoading, setIsLoading] = useState(false);

  const goPrimerUnidad = () => {
    navigation.navigate("pregunta_1");
  };
  const goUnidad = (unidad) => {
    let numPregunta = 1;
    let puntuacion = 0;

    navigation.navigate("preguntas", {
      numPregunta,
      unidad,
      puntuacion
    });
  };

  return (
    <View style={styles.fondo_registro}>
      <Loading isVisible={isLoading} text="Cargando" />

      <View style={styles.banner_bienvenido}>
        <Text style={styles.txt_bienvenido}>Bienvenido </Text>
      </View>
      <View></View>
      <View
        style={{
          width: "100%",
          flexDirection: "row",
          textAlign: "center",
          paddingTop: 80,
        }}
      >
        <View style={{ width: "7%" }}></View>

        <View style={styles_welcome.view_unidad}>
          <TouchableOpacity onPress={goPrimerUnidad}>
            <Image
              style={styles_welcome.image_unidad}
              source={require("../../../assets/img/fondo_menu/fondo_1.jpg")}
            />
            <Text style={styles_welcome.txt_unidad}>
              CIVILIZACIONES FLUVIALES DE LA ANTIGUEDAD
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{ width: "6%" }}></View>
        <View style={styles_welcome.view_unidad}>
          <TouchableOpacity onPress={() => goUnidad(4)}>
            <Image
              style={styles_welcome.image_unidad}
              source={require("../../../assets/img/fondo_menu/fondo_2.jpg")}
            />
            <Text style={styles_welcome.txt_unidad}>GRECIA LA CUNA DE OCCIDENTE</Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={{ width: "100%", flexDirection: "row", paddingTop: 20 }}>
        <View style={{ width: "7%" }}></View>
        <View style={styles_welcome.view_unidad}>
          <TouchableOpacity onPress={() => goUnidad(5)}>
            <Image
              style={styles_welcome.image_unidad}
              source={require("../../../assets/img/fondo_menu/fondo_3.jpg")}
            />
            <Text style={styles_welcome.txt_unidad}>LA CIVILIZACION ROMANA</Text>
          </TouchableOpacity>
        </View>
        <View style={{ width: "6%" }}></View>

        <View style={styles_welcome.view_unidad}>
          <TouchableOpacity onPress={() => goUnidad(5)}>
            <Image
              style={styles_welcome.image_unidad}
              source={require("../../../assets/img/fondo_menu/fondo_4.jpg")}
            />
            <Text style={styles_welcome.txt_unidad}>LA MUJER EN EL ANTIGUO ORIENTE</Text>
          </TouchableOpacity>
        </View>
      </View>
      {/* <View style={{backgroundColor:color_blanco,alignSelf:'flex-end',width:'100%',height:50}}>
                <Text>hola</Text>
                
            </View> */}
    </View>
  );
}
