import React from "react";
import { Image } from "react-native";
import { StyleSheet } from "react-native";
import { TouchableOpacity } from "react-native";
import { ImageBackground } from "react-native";
import { View, Text, SafeAreaView } from "react-native";
import {
  color_azul_fuert,
  color_azul_medio,
  color_blanco,
  color_celeste,
  color_rojo_bajo,
} from "../../../assets/colors/Colores";
import { styles } from "../../../assets/styles/estilos";
import { useNavigation } from "@react-navigation/native";

export default function Puntaje(props) {
  const { route } = props;
  const { puntuacion } = route.params;
  const navigation = useNavigation();

  const salir = () => {
    navigation.reset({
      index: 0,
      routes: [{ name: "home" }],
    });
  };
  return (
    <View style={styles.container_princ}>
      <View style={styles.banner_bienvenido}>
        <Text style={styles.txt_bienvenido}>PUNTAJE </Text>
      </View>
      <ImageBackground
        source={require("../../../../assets/img/menu_principal.jpg")}
        style={styles.image_princ}
      >
        <View style={{ alignSelf: "center", height: "40%" }}>
          <Image
            style={styles.tinyLogo_princ}
            source={require("../../../../assets/img/icons/copa.png")}
          />
        </View>
        <View style={{ alignSelf: "center", flexDirection: "row" }}>
          <TouchableOpacity style={{ width: "70%", paddingTop: 40 }}>
            <View
              style={{
                backgroundColor: color_rojo_bajo,
                height: 70,
                borderRadius: 25,
                justifyContent: "center",
              }}
            >
              <Text
                style={{
                  textAlign: "center",
                  fontSize: 40,
                  color: color_blanco,
                  fontWeight: "bold",
                }}
              >
                {puntuacion}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={{ alignSelf: "center", flexDirection: "row" }}>
          <TouchableOpacity style={{ width: "70%", paddingTop: 40 }} onPress ={salir}>
            <View
              style={{
                backgroundColor: color_celeste,
                height: 70,
                borderRadius: 25,
                justifyContent: "center",
              }}
            >
              <Text
                style={{
                  textAlign: "center",
                  fontSize: 30,
                  color: color_blanco,
                  fontWeight: "bold",
                }}
              >
                Continuar
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    </View>
  );
}

const styless = StyleSheet.create({
  fab: {
    position: "absolute",
    margin: 16,
    right: 0,
    bottom: 0,
  },
});

/*     <SafeAreaView style={{ flex: 1, backgroundColor: color_azul_fuert }}>
      <View style={styles.banner_bienvenido}>
        <Text style={styles.txt_bienvenido}>PUNTAJE </Text>
      </View>

      <View
        style={{
          flex: 1,
          flexDirection: "row",
          justifyContent: "center",
          paddingTop: 60,
          backgroundColor:color_azul_medio
        }}
      >
        <Image
          style={styles.copa_img}
          source={require("../../../../assets/img/icons/copa.png")}
        />
        <Text>hola</Text>
      </View>
      <View>
          <Text>hola</Text>
      </View>
      {/*  <View style={{ width: "100%", alignItems: "center" }}>
        <Image
          style={styles.copa_img}
          source={require("../../../../assets/img/icons/copa.png")}
        />
      </View>
      <View style={{ height: "100%", alignSelf: "center" }}>
        <Text>HAS OBTENIDO </Text>
        <Text>{puntuacion}</Text>
      </View> */
