import React, { useEffect, useState } from "react";
import { TouchableOpacity } from "react-native";
import { StyleSheet } from "react-native";
import { View, Text, ScrollView, Image, Button } from "react-native";
import QuestionApi from "../../../api/QuestionApi";
import {
  color_blanco,
  color_celeste,
  color_res_corr,
  color_res_incor,
} from "../../../assets/colors/Colores";
import { styles } from "../../../assets/styles/estilos";
import {
  EncabezadoPuntuacion,
  EncabezadoUnidad,
  ViewPregunta,
} from "../../../components/Comunes";
import Modal from "../../../components/Modal";
import { useNavigation } from "@react-navigation/native";
import Loading from "../../../components/Loading";

let estado_tiempo = false;
export default function Question(props) {
  const { route } = props;
  const { unidad, numPregunta, puntuacion } = route.params;
  const [enunciado, setEnunciado] = useState(null);
  const [pregunta, setPregunta] = useState(null);
  const [num_quiz, setNumQuiz] = useState(null);
  const [listQuestion, setListQuestion] = useState([]);
  const navigation = useNavigation();
  const [isLoading, setIsLoading] = useState(false);
  const [question, setQuestion] = useState(numPregunta);
  const [tiempo, setTiempo] = useState();
  const [puntosGan, setPuntosGan] = useState(puntuacion);
  const [modalTiempo, setModalTiempo] = useState(false);
  const [stateModal, setStateModal] = useState(false);
  const [imagen, setImagen] = useState(null);

  let tiempito = 59;
  const sendData = () => {
    if (tiempito >= 0) {
      setTiempo(tiempito);
      let tiempoJuego = tiempito - 1;
      tiempito = tiempoJuego;
      if (tiempito == 0 && estado_tiempo === false) {
        console.log(stateModal);
        setModalTiempo(true);
        //setEstadoBtn(false);
      }
    } else {
    }
  };

  useEffect(() => {
    const interval = setInterval(() => {
      sendData();
    }, 1000);
    return () => clearInterval(interval);
  }, []);

  useEffect(() => {
    setIsLoading(true);
    (async () => {
      let tema = await QuestionApi.getTema(unidad);
      let question = await QuestionApi.getPregunta(unidad, numPregunta);
      let num_quiz = await QuestionApi.getNumPregunta(unidad, numPregunta);
      let lista_respuesta = await QuestionApi.getRespuestas(
        unidad,
        numPregunta
      );
      let ruta_img = await QuestionApi.getImagen(unidad, numPregunta);

      setPregunta(question);
      setEnunciado(tema);
      setNumQuiz(num_quiz);
      setListQuestion(lista_respuesta);
      setImagen(ruta_img);

      setIsLoading(false);

    })();
  }, []);

  const salir = (showModal, setShowModal) => {
    setIsLoading(true);
    setShowModal(false);
    setIsLoading(false);
    navigation.reset({
      index: 0,
      routes: [{ name: "home" }],
    });
  };

  const continuar = (contador, setShowModal, puntos, tiempo) => {
    estado_tiempo = true;
    setIsLoading(true);
    if (contador < 10) {
      let numPregunta = contador + 1;
      let totalPuntos = 0;

      if (tiempo == 0) {
        totalPuntos = puntosGan + tiempo;
      } else {
        totalPuntos = puntosGan + puntos;
      }
      let puntuacion = totalPuntos;

      setPuntosGan(puntosGan);

      setQuestion(numPregunta);
      setShowModal(false);
      setIsLoading(false);
      navigation.push("preguntas", {
        numPregunta,
        unidad,
        puntuacion,
      });
    } else {
      navigation.push("puntaje",{puntuacion});
    }
  };
  return (
    <>
      <Loading isVisible={isLoading} text="Cargando" />

      <ScrollView style={styles.fondo_registro}>
        <View style={styles.container}>
          <EncabezadoUnidad titulo={enunciado} />
          <EncabezadoPuntuacion
            pregunta={num_quiz}
            puntos={puntosGan}
            tiempo={tiempo}
          />
          <ViewPregunta pregunta={pregunta} num_quiz={num_quiz} />
          < ViewImages imagen = {imagen} />

          <VistaRespuesta
            respuestas={listQuestion}
            unidad={unidad}
            pregunta={question}
            continuar={continuar}
            salir={salir}
            modalTiempo={modalTiempo}
          />
        </View>
        {modalTiempo && (
          <Modal isVisible={modalTiempo} setIsVisible={setModalTiempo}>
            <View>
              <View style={{ height: 100, alignSelf: "center" }}>
                <Image
                  style={styles.modal_imag}
                  source={require("../../../../assets/img/icons/sad.png")}
                />
              </View>
              <Text style={styles.modal_txt_feli}> La respuesta correcta</Text>
              <Text style={styles.modal_txt_msj_feli}>
                {/* {respuestaCorrecta} */}
              </Text>
              <View style={styles.modal_view_btn_sig}>
                <Button
                  color="#3CCB76"
                  title="Siguiente pregunta"
                  onPress={() =>
                    continuar(question, setModalTiempo, puntosGan, 0)
                  }
                />
              </View>
              <View style={styles.modal_view_btn_salir}>
                <Button
                  color="#E90404"
                  title="Salir"
                  onPress={() => salir(modalTiempo, setModalTiempo)}
                />
              </View>
            </View>
          </Modal>
        )}
      </ScrollView>
    </>
  );
}

function VistaRespuesta(props) {
  const { respuestas, unidad, pregunta, salir, continuar, modalTiempo } = props;
  const [btn1, setBtn1] = useState(color_celeste);
  const [btn2, setBtn2] = useState(color_celeste);
  const [btn3, setBtn3] = useState(color_celeste);
  const [btn4, setBtn4] = useState(color_celeste);
  const [resp1, setResp1] = useState();
  const [resp2, setResp2] = useState();
  const [resp3, setResp3] = useState();
  const [resp4, setResp4] = useState();
  const [est1, setEst1] = useState();
  const [est2, setEst2] = useState();
  const [est3, setEst3] = useState();
  const [est4, setEst4] = useState();
  const [estadoBtn, setEstadoBtn] = useState();
  const [showModal, setShowModal] = useState(false);
  const [showModal2, setShowModal2] = useState(false);
  const [showModal3, setShowModal3] = useState(modalTiempo);

  const [respuestaCorrecta, setRespuestaCorrecta] = useState(false);
  const [showContinuar, setShowContinuar] = useState(false);
  const [puntos, setPuntos] = useState(0);

  useEffect(() => {
    (async () => {
      let opcion1 = await QuestionApi.getRespuestasOpcion(unidad, pregunta, 1);
      setResp1(opcion1);
      let opcion2 = await QuestionApi.getRespuestasOpcion(unidad, pregunta, 2);
      setResp2(opcion2);
      let opcion3 = await QuestionApi.getRespuestasOpcion(unidad, pregunta, 3);
      setResp3(opcion3);
      let opcion4 = await QuestionApi.getRespuestasOpcion(unidad, pregunta, 4);
      setResp4(opcion4);
      let etd1 = await QuestionApi.getRespuestasEstados(unidad, pregunta, 1);
      setEst1(etd1);
      let etd2 = await QuestionApi.getRespuestasEstados(unidad, pregunta, 2);
      setEst2(etd2);
      let etd3 = await QuestionApi.getRespuestasEstados(unidad, pregunta, 3);
      setEst3(etd3);
      let etd4 = await QuestionApi.getRespuestasEstados(unidad, pregunta, 4);
      setEst4(etd4);
      let respuesta = await QuestionApi.getRespuestaCorrecta(unidad, pregunta);
      setRespuestaCorrecta(respuesta);
    })();
  }, []);

  const checkedQuestion = (index, estado) => {
    if (index == 1) {
      if (estado == 1) {
        setBtn1(color_res_corr);
        setShowModal(true);
        setPuntos(100);
      } else {
        setBtn1(color_res_incor);
        setShowModal2(true);
      }
    }
    if (index == 2) {
      if (estado == 1) {
        setBtn2(color_res_corr);
        setShowModal(true);
        setPuntos(100);
      } else {
        setBtn2(color_res_incor);
        setShowModal2(true);
      }
    }
    if (index == 3) {
      if (estado == 1) {
        setBtn3(color_res_corr);
        setShowModal(true);
        setPuntos(100);
      } else {
        setBtn3(color_res_incor);
        setShowModal2(true);
      }
    }
    if (index == 4) {
      if (estado == 1) {
        setBtn4(color_res_corr);
        setShowModal(true);
        setPuntos(100);
      } else {
        setBtn4(color_res_incor);
        setShowModal2(true);
      }
    }
    setEstadoBtn(true);
    //setShowModal(true);
    setShowContinuar(true);
  };

  return (
    <>
      <Modal isVisible={showModal} setIsVisible={setShowModal}>
        <View>
          <View style={{ height: 100, alignSelf: "center" }}>
            <Image
              style={styles.modal_imag}
              source={require("../../../../assets/img/icons/feliz.png")}
            />
          </View>
          <Text style={styles.modal_txt_feli}>Felicitaciones!</Text>
          <Text style={styles.modal_txt_msj_feli}>
            Lo estas haciendo muy bien, sigue jugando
          </Text>

          <View style={styles.modal_view_btn_sig}>
            <Button
              color="#3CCB76"
              title="Siguiente pregunta"
              onPress={() => continuar(pregunta, setShowModal, puntos)}
            />
          </View>
          <View style={styles.modal_view_btn_salir}>
            <Button
              color="#E90404"
              title="Salir"
              onPress={() => salir(showModal, setShowModal)}
            />
          </View>
        </View>
      </Modal>

      <Modal isVisible={showModal2} setIsVisible={setShowModal2}>
        <View>
          <View style={{ height: 100, alignSelf: "center" }}>
            <Image
              style={styles.modal_imag}
              source={require("../../../../assets/img/icons/sad.png")}
            />
          </View>
          <Text style={styles.modal_txt_feli}> La respuesta correcta</Text>
          <Text style={styles.modal_txt_msj_feli}>{respuestaCorrecta}</Text>
          <View style={styles.modal_view_btn_sig}>
            <Button
              color="#3CCB76"
              title="Siguiente pregunta"
              onPress={() => continuar(pregunta, setShowModal2, puntos)}
            />
          </View>
          <View style={styles.modal_view_btn_salir}>
            <Button
              color="#E90404"
              title="Salir"
              onPress={() => salir(showModal2, setShowModal2)}
            />
          </View>
        </View>
      </Modal>

      <Modal isVisible={showModal3} setIsVisible={setShowModal3}>
        <View>
          <View style={{ height: 100, alignSelf: "center" }}>
            <Image
              style={styles.modal_imag}
              source={require("../../../../assets/img/icons/sad.png")}
            />
          </View>
          <Text style={styles.modal_txt_feli}> La respuesta correcta</Text>
          <Text style={styles.modal_txt_msj_feli}>{respuestaCorrecta}</Text>
          <View style={styles.modal_view_btn_sig}>
            <Button
              color="#3CCB76"
              title="Siguiente pregunta"
              onPress={() => continuar(pregunta, setShowModal3, puntos)}
            />
          </View>
          <View style={styles.modal_view_btn_salir}>
            <Button
              color="#E90404"
              title="Salir"
              onPress={() => salir(showModal3, setShowModal3)}
            />
          </View>
        </View>
      </Modal>

      <VistaOpciones
        opcion={resp1}
        btnColor={btn1}
        estado={est1}
        index={1}
        checkedQuestion={checkedQuestion}
        estadoBtn={estadoBtn}
      />
      <VistaOpciones
        opcion={resp2}
        btnColor={btn2}
        estado={est2}
        index={2}
        checkedQuestion={checkedQuestion}
        estadoBtn={estadoBtn}
      />
      <VistaOpciones
        opcion={resp3}
        btnColor={btn3}
        estado={est3}
        index={3}
        checkedQuestion={checkedQuestion}
        estadoBtn={estadoBtn}
      />
      <VistaOpciones
        opcion={resp4}
        btnColor={btn4}
        estado={est4}
        index={4}
        checkedQuestion={checkedQuestion}
        estadoBtn={estadoBtn}
      />
      <View style={{ paddingTop: 50 }}></View>
      {showContinuar && (
        <>
          <TouchableOpacity
            style={{
              backgroundColor: color_res_corr,
              alignSelf: "center",
              alignContent: "center",
              alignItems: "center",
              justifyContent: "center",
              width: 120,
              height: 50,
              borderRadius: 50,
            }}
            onPress={() => continuar(pregunta, setShowModal)}
          >
            <Text style={{ color: color_blanco }}>Siguiente</Text>
          </TouchableOpacity>
        </>
      )}
    </>
  );
}

export function ViewImages(props) {
  const { imagen } = props;
  return (
    <View
      style={{
        width: "100%",
        height: 150,
        alignItems: "center",
        
        marginVertical:15
      }}
    >
      <Image
        style={{
          width: 300,
          height: 150,
          resizeMode: "contain",
        }}
        source={imagen}
      />
    </View>
  );
}

function VistaOpciones(props) {
  const {
    opcion,
    checkedQuestion,
    btnColor,
    respuesta,
    estado,
    id,
    index,
    estadoBtn,
  } = props;
  return (
    <TouchableOpacity
      onPress={() => checkedQuestion(index, estado)}
      disabled={estadoBtn}
    >
      <View
        style={{
          backgroundColor: btnColor,
          height: 50,
          borderRadius: 50,
          width: "80%",
          justifyContent: "center",
          alignSelf: "center",
          marginTop: 20,
        }}
      >
        <Text
          style={{
            color: color_blanco,
            alignSelf: "center",
            fontWeight: "bold",
            fontSize: 16,
          }}
        >
          {opcion}
        </Text>
      </View>
    </TouchableOpacity>
  );
}
