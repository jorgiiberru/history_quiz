import React from "react";
import { View, Text, Image } from "react-native";
import Modal from "../../../components/Modal";

export default function Modales(props) {
  const { showModal, setShowModal } = props;
  return (
    <Modal isVisible={showModal} setIsVisible={setShowModal}>
      <View>
        <View style={{ height: 100, alignSelf: "center" }}>
          <Image
            style={{ width: 90, height: 90, borderRadius: 90 / 2 }}
            source={require("../../../../assets/img/icons/feliz.png")}
          />
        </View>
        <Text style={{ textAlign: "center", fontSize: 25, paddingBottom: 5 }}>
          Felicitaciones!
        </Text>
        <Text style={{ textAlign: "center", paddingBottom: 5 }}>
          Lo estas haciendo muy bien, sigue jugando
        </Text>

        <View
          style={{
            paddingVertical: 10,
            width: "80%",
            alignContent: "center",
            alignSelf: "center",
          }}
        >
          <Button
            color="#3CCB76"
            title="Siguiente pregunta"
            onPress={continuar}
          />
        </View>

        <View
          style={{
            paddingVertical: 10,
            width: "80%",
            alignContent: "center",
            alignSelf: "center",
          }}
        >
          <Button color="#E90404" title="Salir" onPress={salir} />
        </View>
      </View>
    </Modal>
  );
}
