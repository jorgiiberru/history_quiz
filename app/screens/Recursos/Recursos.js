import { FontAwesome } from "@expo/vector-icons";
import React from "react";
import { TouchableOpacity } from "react-native";
import { View, Text } from "react-native";
import { useNavigation } from "@react-navigation/native";

export default function Recursos() {
  const navigation = useNavigation();
  const goFunction = (screen) => {
    navigation.navigate(screen);
  };

  return (
    <View style={{ borderTopWidth: 0.5 }}>
      <DatosEstudiante
        icono="book"
        texto="Libro"
        seleccion="Libro"
        goFunction={goFunction}
      />
      <DatosEstudiante
        icono="video-camera"
        texto="Tutorial"
        seleccion="Tutorial"
        goFunction={goFunction}
      />
      <DatosEstudiante
        icono="youtube-play"
        texto="Promocional"
        seleccion="Promocional"
        goFunction={goFunction}
      />
    </View>
  );
}

function DatosEstudiante(props) {
  const { icono, texto, goFunction, seleccion } = props;
  return (
    <TouchableOpacity onPress={() => goFunction(seleccion)}>
      <View
        style={{
          height: 60,
          borderBottomWidth: 0.5,
          justifyContent: "center",
          alignContent: "center",
          flexDirection: "row",
          alignItems: "center",
        }}
      >
        <View style={{ width: "10%" }}>
          <FontAwesome name={icono} size={24} color="black" />
        </View>
        <View style={{ width: "80%" }}>
          <Text>{texto}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
}
