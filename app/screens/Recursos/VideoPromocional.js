import { WebView } from "react-native-webview";
import React from "react";

export default function VideoPromocional() {
  var url =
    "https://drive.google.com/file/d/13EhDN_aYy4F79cw7qvLSes7d8sN1k6Aq/view?usp=sharing";
  return (
    <WebView
      javaScriptEnabled={true}
      domStorageEnabled={true}
      source={{ uri: url }}
    />
  );
}
